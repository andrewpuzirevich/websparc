<?php

namespace wpsync;

class Crud
{
    /**
     * @param $sku
     * @return int|null
     */
    public function checkSKU($sku): ?int
    {
        $prodID = wc_get_product_id_by_sku($sku);
        if ($prodID){
            return $prodID;
        } else {
            return null;
        }
    }

    /**
     * @param $productArray
     * @param $productID
     */
    public function updateProduct($productArray,$productID): int
    {
        $product = new \WC_Product_Simple($productID);

        if (isset($productArray['name']) and $productArray['name'] != $product->get_name()){
            $product->set_name($productArray['name']);
        }
        if (isset($productArray['description']) and $productArray['description'] != $product->get_description()){
            $product->set_description($productArray['description']);
        }

        if (isset($productArray['picture'])){
            if ( ! function_exists( 'download_url' ) ) {
                require_once ABSPATH . 'wp-admin/includes/file.php';
            }
            $oldImgID = $product->get_image_id();
            $oldImg = wp_get_attachment_image_url($oldImgID,"full");
            $filenameOld = pathinfo($oldImg, PATHINFO_FILENAME);

            $tmp = download_url( $productArray['picture'] );
            $filenameNew = pathinfo($tmp, PATHINFO_FILENAME);
            if ($filenameOld != $filenameNew){
                wp_delete_attachment($oldImgID);

                $imgID = $this->uploadPicture($productArray['picture'],$productArray['name']);
                if ($imgID){
                    $product->set_image_id($imgID);
                }
            }

        }
        if (isset($productArray['in_stock']) and $productArray['in_stock'] != $product->get_stock_quantity()){
            $product->set_stock_quantity( (int)$productArray['in_stock'] );
        }

        if (isset($productArray['price'])){
            $price = substr($productArray['price'],1);
            if ($price != $product->get_regular_price()){
                $product->set_regular_price($price);
            }
        }

        return $product->save();
    }

    /**
     * @param $productArray
     * @return int
     * @throws \WC_Data_Exception
     */
    public function createProduct($productArray): int
    {
        set_time_limit(600);
        $product = new \WC_Product_Simple();
        if (isset($productArray['name'])){
            $product->set_name($productArray['name']);
        }
        if (isset($productArray['description'])){
            $product->set_description($productArray['description']);
        }
        $product->set_status('publish');
        if (isset($productArray['sku'])){
            $product->set_sku($productArray['sku']);
        }
        if (isset($productArray['picture'])){
            $imgID = $this->uploadPicture($productArray['picture'],$productArray['name']);
            if ($imgID){
                $product->set_image_id($imgID);
            }
        }
        $product->set_manage_stock( true );
        if (isset($productArray['in_stock'])){
            $product->set_stock_quantity( (int)$productArray['in_stock'] );
        }

        if (isset($productArray['price'])){
            $price = substr($productArray['price'],1);
            $product->set_regular_price($price);
        }

        return $product->save();
    }

    /**
     * @param $url
     * @param $name
     * @return int
     */
    private function uploadPicture($url, $name): int
    {
        require_once( ABSPATH . "/wp-load.php");
        require_once( ABSPATH . "/wp-admin/includes/image.php");
        require_once( ABSPATH . "/wp-admin/includes/file.php");
        require_once( ABSPATH . "/wp-admin/includes/media.php");

        $tmp = download_url( $url );
        if ( is_wp_error( $tmp ) ) return false;

        $filename = pathinfo($url, PATHINFO_FILENAME);
        $extension = pathinfo($url, PATHINFO_EXTENSION);

        if ( ! $extension ) {
            $mime = mime_content_type( $tmp );
            $mime = is_string($mime) ? sanitize_mime_type( $mime ) : false;

            $mime_extensions = array(
                'image/jpg'          => 'jpg',
                'image/jpeg'         => 'jpeg',
                'image/gif'          => 'gif',
                'image/png'          => 'png',
            );

            if ( isset( $mime_extensions[$mime] ) ) {
                $extension = $mime_extensions[$mime];
            }else{
                @unlink($tmp);
                return false;
            }
        }

        $args = array(
            'name' => "$filename.$extension",
            'tmp_name' => $tmp,
        );

        $attachment_id = media_handle_sideload( $args, 0, $name);

        @unlink($tmp);

        if ( is_wp_error($attachment_id) ) return false;

        return (int)$attachment_id;
    }

    public function deleteProduct($products)
    {
        global $wpdb;
        $existProductsSKU = $wpdb->get_results("SELECT post_id,meta_value FROM `{$wpdb->prefix}postmeta` WHERE meta_key='_sku'",ARRAY_A);

        $skuArrayAPI = array();
        foreach ($products as $product) {
            array_push($skuArrayAPI,$product['sku']);
        }

        foreach ($existProductsSKU as $item) {
            if (!in_array($item['meta_value'],$skuArrayAPI)){
                wp_delete_attachment($item['post_id']);
                wp_delete_post($item['post_id']);
            }
        }
    }
}