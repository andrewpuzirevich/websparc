<?php

namespace wpsync;

class Main
{

    /**
     * hourly event
     * @throws \WC_Data_Exception
     */
    public function hourlyProductCheck($attempt = null)
    {
        if (!$attempt){
            $attempt = 0;
        } else {
            $attempt +=1;
        }

        $products = $this->getProducts();
        if (!$products['error']){
            $this->processProducts($products['data']);
        } else {
             $this->errorsResolve($products['code'],$attempt);
        }
    }

    /**
     * @throws \WC_Data_Exception
     */
    private function processProducts($products)
    {
        set_time_limit(600);
        $crud = new Crud();

        foreach ($products as $product){

            $productID = $crud->checkSKU($product['sku']);
            if ($productID != null){
                if (!$crud->updateProduct($product,$productID)){
                    $this->errorLogs(400, 'Can`t update product ID '.$productID);
                }
            } else {
                if ($crud->createProduct($product)){
                    $this->errorLogs(400, 'Can`t create product');
                }
            }
        }

        $crud->deleteProduct($products);
    }


    /**
     * @param $code
     * @param $attempt
     */
    private function errorsResolve($code,$attempt)
    {
        switch ($code){
            case 429:
                if ($attempt <= 3){
                    sleep(60);
                    $this->hourlyProductCheck();
                } else {
                    $this->errorLogs($code,'Incorrect api answer after 3 attempts');
                }
                break;
            default:
                $this->errorLogs($code,'Incorrect api answer');
                break;
        }
    }

    /**
     * @param $code
     * @param $text
     */
    private function errorLogs($code,$text)
    {
        $file = WPSYNC_DIR."\log.txt";

        $handle = fopen($file, "a+");

        $time = date('d.m.Y H:i:s',time());

        $str = "$time: $text, server code is: $code.\n";

        fwrite($handle, $str);

        fclose($handle);
    }

    /*
     * curl to try to get products
     */
    private function getProducts()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://wp.webspark.dev/wp-api/products');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        $headers = array();
        $headers[] = 'Accept: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return 'Error:' . curl_error($ch);
        }
        $info = curl_getinfo($ch);
        curl_close($ch);

        if ($info['http_code'] == 200){
            return json_decode($result, true);
        } else{
            return array('error'=>'true','code'=>$info['http_code']);
        }
    }

//db renew

// check sku (crud -> create\update\delete)


}