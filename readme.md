If you have a plugin in the form of a zip archive:

The plugin is installed in the WordPress "Plugins" menu, then "add new", then "download". Select the plugin archive and download it, then activate it.
After activation, the plugin itself will create a cron task that will run 1 time per hour, performing the necessary tasks.

If you download the plugin folder:

Place the folder with the plugin in the standard WordPress directory ("your_site\wp-content\plugins")

The plugin works with WordPress 6.2, PHP 8.1
WooCommerce must be installed
Cron must be enabled