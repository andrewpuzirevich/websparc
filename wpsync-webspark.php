<?php
/**
 * Plugin Name: WPSync Webspark
 * Description: WPSync developed for Webspark Product API. Product CRUD & Sync.
 * Version: 1.0.1
 */

define('WPSYNC_DIR', __DIR__);
define("WPSYNC_URL", plugins_url().'/wpsync-webspark/');

require_once (WPSYNC_DIR.'/includes/loader.php');